# NIR Birefringence Analysis

## Stokes Parameters Measurements

| m | Analyzer | Waveplate | Calculations |
|:---:|:--------:|:---------:|:---:|
| $m_1$ | H (174) | H (5) | $S_0 = m_1 + m_2$ |
| $m_2$ | V (84) | V (95) | $S_1 = m_1 - m_2$ |
| $m_3$ | D (129) | D (50) | $S_2 = 2\cdot m3 - S_0$ |
| $m_4$ | H (174) | D (5) | $S_3 = S_0 - 2\cdot m_4$ |

*https://pol3he.sites.wm.edu/wp-content/uploads/sites/50/2020/01/measuring-Stokes-parameters.pdf*

$S_0 = m_1 + m_2$

$S_1 = m_1 - m_2$

$S_2 = 2\cdot m3 - S_0$ 

$S_3 = S_0 - 2\cdot m_4$

*https://en.wikipedia.org/wiki/Stokes_parameters#Properties*

polarization: $p = \frac {\sqrt {S_1^2 + S_2^2 + S_3^2}} {S_0}$ 

## Birefrigence Calculations

*P. C. Chen, Y. L. Lo S Using polarimeter and Stokes parameters for measuring linear birefringence and diattenuation properties of optical samples*
 
birefrigence principle axis: $\alpha = \frac{1}{2}\arctan \left(\frac{-S_{3,0}}{S_{3,45}} \right)$ 

birefrigence retardance: $\beta = \arctan \left ( \frac {S_{3,45}}{\cos{\left ( 2\alpha \right )} \space \cdot \space S_{3,RHC}} \right )$

birefrigence index: $\Delta n = \frac {\beta \lambda} {2\pi d}$