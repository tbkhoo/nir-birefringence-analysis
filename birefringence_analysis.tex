\documentclass[10pt, letterpaper]{article}
% \usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{float}
\usepackage{subcaption}

\usepackage[letterpaper, left=1in, right=1in, bottom=1in, top=1in]{geometry}

% chktex-file 2
% chktex-file 8 
% chktex-file 45 
% chktex-file 46

\graphicspath{ {./tim_measurements/figures/} }

\title{Birefringence Analysis with Near-Infrared Light}
\author{Timothy Khoo}
\date{May--August 2022}

\begin{document}

\maketitle

\begin{abstract}
This paper investigates a methodology to measure the birefringence of thin silicon crystal samples. This method revolves around the use of polarized near-infrared light (1550nm) to image the samples. It is able to successfully resolve the sample edges, texture and defects. 
\end{abstract}

\tableofcontents

\clearpage

\section{Introduction}
Optical materials will sometimes have distinct indices of refraction that depend on the polarization of incoming light. The maximum differences between these indices of refraction is known as the birefringence, and for crystalline silicon varies on the order of $10^{-6}$ to $10^{−4}$ depending on the direction of light relative to the crystal structure. An understanding and measurement of birefringence is important, because any birefringence will induce effects on incoming light depending on its polarization. Specifically, it may rotate the orientation of the light, which is important to understand in optical experiments. The focus of this work is to develop and understand methods to measure this birefringence (which can be very small) various silicon samples. Eventually, we aim to apply these methods to characterize silicon samples for interferometry experiments.

\section{Theory}

\subsection{Birefringence}

Birefringence is often represented numerically as the birefringence index, $\Delta n$. It is defined as the difference between the index of refraction for the ordinary ray, $n_o$ and the index of refraction of the extraordinary ray, $n_e$, shown in equation \eqref{birefringence index}

\begin{equation}\label{birefringence index}
	\Delta n = n_o - n_e
\end{equation}

Birefringence can be computed using equation \eqref{birefringence}, where $\beta$ is retardation angle in radians, $\lambda$ is the wavelength of light in m, and $d$ is physical path length in m, which in this case is the sample thickness. 

\begin{equation}\label{birefringence}
	\Delta n = \frac{\beta\lambda}{2\pi d}
\end{equation}

\subsection{Rotation Method - Transmittance Model}

The rotation method of measuring birefringence involves using a curve fitting technique to a transmittance model. The transmittance, $T$, can be measurement directly through depolarization, $D$, in equation \eqref{Transmittance Measurement}. Note that in this equation only, depolarization encompasses all forms of lost intensity of light, including reflection, absorption and loss of polarization due to scattering. Throughout the rest of this report, depolarization will henceforth only refer to the \textit{loss} of polarization, typically expressed as a percentage of the light that change from purely polarized light to incoherent or natural light.

\begin{equation}\label{Transmittance Measurement}
	D = 1 - \frac{I_\parallel - I_\perp}{I_\parallel + I_\perp}
	= 2 \frac{I_\perp}{I_\parallel + I_\perp} = 2T
\end{equation}

Equation \eqref{Transmittance Model} shows the transmittance model, relating $T\left(\theta\right)$ to $\beta$, the retardation angle and $\alpha$, the birefringence fast-axis angle. Notice that $T\left(\theta\right)$ is dependent on $\theta$, the polarizer angle, whose origin is defined at the horizontal. As such, $\alpha$ will describe be the angle between the horizontal, and the fast-axis angle. A key characteristic of this equation is that it is 90 degrees periodic, regardless of $\beta$ and $\alpha$. Any vertical offset will be reflected the depolarization parameter $D$, which in this case refers only to the \textit{loss} of polarization as mentioned above. Horizontal offsets will be reflected by changes in $\alpha$, while changes in amplitude will affect $\beta$. 

\begin{equation}\label{Transmittance Model}
	T\left(\theta\right) = D + \sin^2{\left(\frac{\beta}{2}\right)}
	\sin^2{\left(2\left(\theta-\alpha\right)\right)}\left[1-D\right]
\end{equation}

With both a method to measure $T\left(\theta\right)$ and to relate it with the relevant birefringence parameters, equating \eqref{Transmittance Measurement} and \eqref{Transmittance Model} yields the final equation \eqref{Transmittance}: 

\begin{equation}\label{Transmittance}
	\frac{I_\perp}{I_\parallel + I_\perp}
	= D + \sin^2{\left(\frac{\beta}{2}\right)}
	\sin^2{\left(2\left(\theta-\alpha\right)\right)}\left[1-D\right]
\end{equation}

The objective of this experiment then becomes apparent: compute the birefringence parameters by performing a curve fit to equation \eqref{Transmittance}. 

\subsection{Stokes Method}

\section{Samples}

\subsection{Thin Sample 1}

\begin{figure}[H]
	\centering
	\begin{subfigure}{0.49\textwidth}
	\centering
	\includegraphics[width = \textwidth]{machined-sample-1-front/sample-1-front}
	\caption{Front\label{fig:sample-1-front}}
	\end{subfigure}
	\begin{subfigure}{0.49\textwidth}
	\centering
	\includegraphics[width = \textwidth]{machined-sample-1-rear/sample-1-rear}
	\caption{Back\label{fig:sample-1-rear}}
	\end{subfigure}
	\caption{Thin Sample 1\label{fig:sample-1-images}}
\end{figure}

The first sample (Figure \ref{fig:sample-1-images}) tested is a thin sample of about 100 microns throughout. The surfaces on both sides are quite smooth and even, without any obvious signs of damage. However, there are some white areas that contain small amounts of glue. 

\subsection{Wedge Sample 2}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{machined-sample-2/sample-2}
	\caption{Wedge Sample 2\label{fig:sample-2-image}}
\end{figure}

The second sample tested (Figure \ref{fig:sample-2-image}) is a wedge sample with decreasing thickness from one side to another, reaching the single digit microns in thickness. 

\subsection{Baby Interferometer Sample 3}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{baby-interferometer/baby-interferometer}
	\caption{Wedge Sample 2\label{fig:sample-3-image}}
\end{figure}

The third and final sample tested is a baby interferometer. It is a sample which consists of 3 pieces of silicon stacked in a row, with the middle piece extending further out than the first and last pieces. Due to movement constraints of the kozhu motors, this sample is scanned in two halves: right and left sides. In the top left corner, there is a small chip and some minor scratches along the surface in the centre. 


\section{Experimental Setup}
	\begin{figure}[H]
		\centering
		\includegraphics[width=\textwidth]{experimental_setup}
		\caption{NIR Beam Setup.\label{fig:setup}}
	\end{figure}

The entire experimental setup is placed within a dark box, covered with black curtains to prevent interference from the ambient infrared light of the sun. Refer to Figure~\ref{fig:setup} for a diagram of the experimental setup. 

This experimental setup uses the ThorLabs S4FC 1550nm Fabry-Perot BenchTop Laser Source (with adjustable power output up to 50mW) coupled via fiber optic cable to a ThorLabs TC06FC-1550 coupler. The laser power output drifts slightly over time when first powered on, but remains stable after about 24 hours. To avoid this, the laser is kept on for as long as possible unless required to be shut off. In that case, measurements do not resume until it has been turned back on and remains so for at least 24 hours. The longest experiments can take up to 24 hours to perform, over which the laser power is stable to $<0.2\%$ if it has been on for at least 24 hours. 

% TODO: add proper product names and uncertainties.
After the laser there is a coupled linear polarizer and quarter waveplate to normalize the intensity of the laser given that its not a perfect incoherent light source. Then there is a polarizer mounted to a ThorLabs motor, which should output a consistent power regardless of its orientation thanks to the waveplate. Afterwards, there is a sample mounted on a 6-axis Kohzu motor: translation and rotation in x, y, z. This allows for the sample to be moved freely to perform the necessary scans. Following the sample, is an optional quarter waveplate mounted to a ThorLabs motor only used with the stokes method, then an analyzer also mounted to a ThorLabs motor. 

Finally, the beam passes through a pin hole with a diameter of about $2.5\pm0.1\mu$m before reaching a ThorLabs S132C power meter, coupled to the ThorLabs PM100D power meter console. According to the ThorLabs spec sheet, the power meter has a resolution of 1 nW, with an error of about $\pm5\%$. In reality, the noise floor sits at around 80-90nW and distinctions of less than 10nW cannot be easily made. However, using a large number of measurements to compute statistical uncertainty can improve precision of the measurements. Each power meter measurement is an average of 100 measurements taken in quick succession, with the error taken as the standard deviation of the set of 100 measurements. Typically, this error falls within $<1\%$, much better than the stated $5\%$.

There is also a camera behind the power meter accessed by lowering the power meter to allow the beam to reach the back. It can be helpful in optically determining the bounds of a scan. 

The experimental setup is operated electronically, through a desktop computer connected to all the devices, like the Kohzu and ThorLabs motors. While these devices could be controlled through their manufacturer's proprietary software, there were also python wrapper packages available for higher level automation of data collection. Primarily jupyter notebooks were used and large quantities of data were collected and stored using the Pandas DataFrame modules in csv files. The notebooks and data files can be found on the desktop computer at the IQC in RAC2 room 2004. 


\section{Measurements and Data Analysis}

Data analysis is performed using notebooks which can be found a git repository named nir-birefringence-analysis on GitLab. Instructions to use the notebooks for data analysis can be found at the beginning of the file. 

The general process for analysing the data follows a few steps. First, there is a configuration stage, where filenames of data, parameters such as sample thickness are entered. Then, there is the data extraction stage where the large amounts of data are processed into qexpy objects, a python package that helps to calculate and propagate uncertainties. Afterwards, for every point of measurement (each box in the 2D heat maps seen below), the program attempts to fit the data to equation \eqref{Transmittance} using the scipy.optimize package. That means there is a unique curve fit for every point in the scan. Finally, the fit parameters,$\alpha$, $\beta$, and $D$, are then used to generate their respective plots. $\beta$ is also used to calculate $\Delta n$ which is also used to generate a plot. 

\subsection{Waveplate Control Tests}

To determine the validity of model, we perform control tests perform using waveplates with known birefringence values. Theoretical values are then compared with the measured values to verify precision of our experimental setup. 

\subsubsection{532nm Quarter Waveplate}

The first control tests uses a Zero-Order 532nm Quarter Waveplate as a sample. According to the ThorLabs manual, their Zero-Order Quarter Waveplates are made by combining two Multi-Order Crystalline Quartz Waveplates to obtain an optical path length difference of $\lambda/4$. The fast axis of the first plate is aligned with the slow axis of the second such that the resulting retardance is the difference between each individual plate's retardance. The waveplate has a retardance accuracy of $<\lambda/300$ for the designed wavelength of 532nm.
% TODO: reference: {https://www.thorlabs.com/newgrouppage9.cfm?objectgroup_id=7234}
Using the known properties of the waveplate and equation \eqref{birefringence}, the thickness can be computed to be $14.46\pm1.3\%\mu$ m. At a wavelength of 1550nm, the index of birefringence $(\delta n)$ of quartz is $0.008455$. A measured value of $\beta$ using the experimental setup can be used to compute $\delta n$, and therefore determine the precision of our measurement technique.

The measurements of the 532nm quarter waveplate sample yields a result of $\delta n = 0.00837 \pm 0.00012$, which is within error of the expected value of $0.008455$. 

% \begin{figure}[h]
% 	\centering
% 	\includegraphics[width=\textwidth]{waveplate-control-532-quarter/rotation/birefringence_alpha.png}
% 	\caption{532nm Quarter Waveplate $\alpha$.\label{fig:532-quarter-waveplate-alpha}}
% \end{figure}
% \begin{figure}[h]
% 	\centering
% 	\includegraphics[width=\textwidth]{waveplate-control-532-quarter/rotation/birefringence_beta.png}
% 	\caption{532nm Quarter Waveplate $\beta$.\label{fig:532-quarter-waveplate-beta}}
% \end{figure}
% \begin{figure}[h]
% 	\centering
% 	\includegraphics[width=\textwidth]{waveplate-control-532-quarter/rotation/birefringence_delta_n.png}
% 	\caption{532nm Quarter Waveplate $\Delta n$.\label{fig:532-quarter-waveplate-delta-n}}
% \end{figure}
% \begin{figure}[h]
% 	\centering
% 	\includegraphics[width=\textwidth]{waveplate-control-532-quarter/rotation/depolarization.png}
% 	\caption{532nm Quarter Waveplate Depolarization.\label{fig:532-quarter-waveplate-depolarization}}
% \end{figure}

\subsubsection{532nm Half Waveplate}

The half waveplate control test is nearly identical to the quarter waveplate test above, with the only differences in a retardance of $\lambda/2$ instead of $\lambda/4$ and a thickness of $d=28.92\pm1.3\%\mu$ m instead of $d=14.46\pm1.3\%\mu$ m. 

The measurements of the 532nm half waveplate sample yields a result of $\delta n = 0.00864 \pm 0.00013$, which is outside of error of the expected value of $0.008455$ by <1\%. 

% \begin{figure}[h]
% 	\centering
% 	\includegraphics[width=\textwidth]{waveplate-control-532-half/rotation/birefringence_alpha}
% 	\caption{532nm Half Waveplate $\alpha$.\label{fig:532-half-waveplate-alpha.png}}
% \end{figure}
% \begin{figure}[h]
% 	\centering
% 	\includegraphics[width=\textwidth]{waveplate-control-532-half/rotation/birefringence_beta}
% 	\caption{532nm Half Waveplate $\beta$.\label{fig:532-half-waveplate-beta.png}}
% \end{figure}
% \begin{figure}[h]
% 	\centering
% 	\includegraphics[width=\textwidth]{waveplate-control-532-half/rotation/birefringence_delta_n}
% 	\caption{532nm Half Waveplate $\Delta n$.\label{fig:532-half-waveplate-delta-n.png}}
% \end{figure}
% \begin{figure}[h]
% 	\centering
% 	\includegraphics[width=\textwidth]{waveplate-control-532-half/rotation/depolarization}
% 	\caption{532nm Half Waveplate Depolarization.\label{fig:532-half-waveplate-depolarization.png}}
% \end{figure}

\subsubsection{1550nm Half Waveplate}

Control tests were attempted with the 1550nm half waveplate sample. However due to its unique properties of $\beta=180^{\circ}$, the analytical techniques failed to properly interpret the data. While this is a major concern, it is not anticipated that the silicon samples to be used would have such high amounts of retardance due to their extremely low thickness of <1mm.

\subsection{Silicon Sample Tests}

For every sample, the data is presented through a series of plots. The first set of plots of each sample are intensity plots. These plots show the raw data measured by the power meter in both the polarizer aligned and antialigned configurations. They are helpful in revealing the positioning of the sample in the scans with respect to the plots. Following the intensity plots, there are plots that show the following computed birefringence parameters: $\alpha$ the birefringence fast-axis angle; $\beta$ the birefringence retardance amount; $\Delta n$ the birefringence index; $D$ the depolarization which refers to the loss of polarization.  

\subsubsection{Thin Sample 1}

This sample was scanned on both the front and rear sides, so there are two sets of plots. The data for this sample is quite good, and the quality of the fits for equation \eqref{Transmittance} is overall acceptable. The data for the plots can be seen in figures \ref{fig:sample-1-front-plots} and \ref{fig:sample-1-rear-plots}. There are not many noteworthy points, aside from slightly elevated values of birefringence at the sample edges. This is particularly evident where the silicon is attached to the mount, most likely due to higher levels of strains where the silicon makes contact and bears the weight of the rest of the sample.  

\begin{figure}[H]
	\centering
	\begin{subfigure}{0.49\textwidth}
	\centering
	\includegraphics[width = \textwidth]{machined-sample-1-front/rotation/intensity_aligned.png}
	\caption{Aligned Intensity\label{fig:sample-1-front-aligned}}
	\end{subfigure}
	\begin{subfigure}{0.49\textwidth}
	\centering
	\includegraphics[width = \textwidth]{machined-sample-1-front/rotation/intensity_antialigned.png}
	\caption{Antialigned Intensity\label{fig:sample-1-front-antialigned}}
	\end{subfigure}
	\caption{Thin Sample 1 Front Intensity Plots\label{fig:sample-1-front-intensity}}
\end{figure}

\begin{figure}[H]
	\begin{subfigure}{\linewidth}
		\centering
		\begin{subfigure}{0.49\textwidth}
			\centering
			\includegraphics[width = \textwidth]{machined-sample-1-front/rotation/birefringence_alpha.png}
			\caption{Birefringence Axis Angle $\alpha$\label{fig:sample-1-front-alpha}}
		\end{subfigure}
		\begin{subfigure}{0.49\textwidth}
			\centering
			\includegraphics[width = \textwidth]{machined-sample-1-front/rotation/birefringence_beta.png}
			\caption{Birefringence Retardance $\beta$\label{fig:sample-1-front-beta}}
		\end{subfigure}
	\end{subfigure}\par\medskip
	\begin{subfigure}{\linewidth}
		\centering
		\begin{subfigure}{0.49\textwidth}
			\centering
			\includegraphics[width = \textwidth]{machined-sample-1-front/rotation/depolarization.png}
			\caption{Depolarization (loss of polarization) $D$\label{fig:sample-1-front-depolarization}}
		\end{subfigure}
		\begin{subfigure}{0.49\textwidth}
			\centering
			\includegraphics[width = \textwidth]{machined-sample-1-front/rotation/birefringence_delta_n.png}
			\caption{Birefringence Index $\Delta n$\label{fig:sample-1-front-delta-n}}
		\end{subfigure}
	\end{subfigure}
	\caption{Thin Sample 1 Front Plots\label{fig:sample-1-front-plots}}
\end{figure}

\begin{figure}[H]
	\centering
	\begin{subfigure}{0.49\textwidth}
	\centering
	\includegraphics[width = \textwidth]{machined-sample-1-rear/rotation/intensity_aligned.png}
	\caption{Aligned Intensity\label{fig:sample-1-rear-aligned}}
	\end{subfigure}
	\begin{subfigure}{0.49\textwidth}
	\centering
	\includegraphics[width = \textwidth]{machined-sample-1-rear/rotation/intensity_antialigned.png}
	\caption{Antialigned Intensity\label{fig:sample-1-rear-antialigned}}
	\end{subfigure}
	\caption{Thin Sample 1 Rear Intensity Plots\label{fig:sample-1-rear-intensity}}
\end{figure}

\begin{figure}[H]
	\begin{subfigure}{\linewidth}
		\centering
		\begin{subfigure}{0.49\textwidth}
			\centering
			\includegraphics[width = \textwidth]{machined-sample-1-rear/rotation/birefringence_alpha.png}
			\caption{Birefringence Axis Angle $\alpha$\label{fig:sample-1-rear-alpha}}
		\end{subfigure}
		\begin{subfigure}{0.49\textwidth}
			\centering
			\includegraphics[width = \textwidth]{machined-sample-1-rear/rotation/birefringence_beta.png}
			\caption{Birefringence Retardance $\beta$\label{fig:sample-1-rear-beta}}
		\end{subfigure}
	\end{subfigure}\par\medskip
	\begin{subfigure}{\linewidth}
		\centering
		\begin{subfigure}{0.49\textwidth}
			\centering
			\includegraphics[width = \textwidth]{machined-sample-1-rear/rotation/depolarization.png}
			\caption{Depolarization (loss of polarization) $D$\label{fig:sample-1-rear-depolarization}}
		\end{subfigure}
		\begin{subfigure}{0.49\textwidth}
			\centering
			\includegraphics[width = \textwidth]{machined-sample-1-rear/rotation/birefringence_delta_n.png}
			\caption{Birefringence Index $\Delta n$\label{fig:sample-1-rear-delta-n}}
		\end{subfigure}
	\end{subfigure}
	\caption{Thin Sample 1 Rear Plots\label{fig:sample-1-rear-plots}}
\end{figure}

\subsubsection{Wedge Sample 2}

Unfortunately, due to the changing thickness of the sample, the exact values cannot be known, thus the birefringence value cannot be computed. This sample's scan showcases many of the shortcomings of the experimental methodology. In particular, a large number of points features poor curve fits. The reason for some of these failed curve is due to much higher values of transmittance than normal, such as in figure \ref{fig:sample-2-fit-bad-20}, where it hovers between 0.34 and 0.36. Typically, this value is much lower like in figures \ref{fig:sample-2-fit-good-3} and \ref{fig:sample-2-fit-good-5}, where it is around 0.002. The elevated transmittance is a result of aligned and antialigned being quite close in values according to equation \eqref{Transmittance}.

\begin{figure}[H]
	\centering
	\begin{subfigure}{0.49\textwidth}
	\centering
	\includegraphics[width = \textwidth]{machined-sample-2/rotation/intensity_aligned.png}
	\caption{Aligned Intensity\label{fig:sample-2-aligned}}
	\end{subfigure}
	\begin{subfigure}{0.49\textwidth}
	\centering
	\includegraphics[width = \textwidth]{machined-sample-2/rotation/intensity_antialigned.png}
	\caption{Antialigned Intensity\label{fig:sample-2-antialigned}}
	\end{subfigure}
	\caption{Wedge Sample 2 Intensity Plots\label{fig:sample-2-intensity}}
\end{figure}

\begin{figure}[H]
	\centering
	\begin{subfigure}{0.32\textwidth}
		\centering
		\includegraphics[width = \textwidth]{machined-sample-2/rotation/birefringence_alpha.png}
		\caption{Birefringence Axis Angle $\alpha$\label{fig:sample-2-alpha}}
	\end{subfigure}
	\begin{subfigure}{0.32\textwidth}
		\centering
		\includegraphics[width = \textwidth]{machined-sample-2/rotation/birefringence_beta.png}
		\caption{Birefringence Retardance $\beta$\label{fig:sample-2-beta}}
	\end{subfigure}
	\begin{subfigure}{0.32\textwidth}
		\centering
		\includegraphics[width = \textwidth]{machined-sample-2/rotation/depolarization.png}
		\caption{Depolarization $D$\label{fig:sample-2-depolarization}}
	\end{subfigure}
	\caption{Wedge Sample 2 Rear Plots\label{fig:sample-2-plots}}
\end{figure}

\begin{figure}[H]
	\begin{subfigure}{\linewidth}
		\centering
		\begin{subfigure}{0.35\textwidth}
			\centering
			\includegraphics[width = \textwidth]{machined-sample-2/rotation/curvefit/beta_3_good.png}
			\caption{Excellent fit of $\beta=3$\label{fig:sample-2-fit-good-3}}
		\end{subfigure}
		\begin{subfigure}{0.32\textwidth}
			\centering
			\includegraphics[width = \textwidth]{machined-sample-2/rotation/curvefit/beta_3_mid.png}
			\caption{Mediocre fit of $\beta=3$\label{fig:sample-2-fit-mid-3}}
		\end{subfigure}
		\begin{subfigure}{0.3\textwidth}
			\centering
			\includegraphics[width = \textwidth]{machined-sample-2/rotation/curvefit/beta_6_bad.png}
			\caption{Poor fit of $\beta=6$\label{fig:sample-2-fit-bad-6}}
		\end{subfigure}
	\end{subfigure}\par\medskip
	\begin{subfigure}{\linewidth}
		\centering
		\begin{subfigure}{0.35\textwidth}
			\centering
			\includegraphics[width = \textwidth]{machined-sample-2/rotation/curvefit/beta_5_good.png}
			\caption{Excellent fit of $\beta=5$\label{fig:sample-2-fit-good-5}}
		\end{subfigure}
		\begin{subfigure}{0.32\textwidth}
			\centering
			\includegraphics[width = \textwidth]{machined-sample-2/rotation/curvefit/beta_1_mid.png}
			\caption{Mediocre fit of $\beta=1$\label{fig:sample-2-fit-mid-1}}
		\end{subfigure}
		\begin{subfigure}{0.3\textwidth}
			\centering
			\includegraphics[width = \textwidth]{machined-sample-2/rotation/curvefit/beta_20_bad.png}
			\caption{Poor fit of $\beta=20$\label{fig:sample-2-fit-bad-20}}
		\end{subfigure}
	\end{subfigure}
	\caption{Thin Sample 1 Front Plots\label{fig:sample-2-curvefit}}
\end{figure}

\subsubsection{Baby Interferometer Sample 3}

As mentioned above, the baby interferometer is too large to scanned all at once and must be split into right and left halves. Unfortunately, the thicknesses of the sample is not known, especially as there are sections where the beam passes through 3 consecutive pieces of silicon. 

A large majority of points have excellent curve fits, with the rest having acceptable fits. There were no outliers in the fits. Looking at the $\beta$ plots (figures \ref{fig:sample-3-right-beta} and \ref{fig:sample-3-left-beta}), the sections in the middle with 3 layers of silicon have approximately 3 times more retardance than the outer sections with only 1 layer of silicon. This correctly reflects our understanding of birefringence that tripling the thickness triples the retardance. Along the outer edges of the sample, there is an uptick in the $\beta$ values. This can most likely be attributed to poorer quality of machining technique and consistency along the edges. Another point of interest can be found in the top left corner of the sample in figure \ref{fig:sample-3-left-beta}, where a point of very high birefringence is present. When cross comparing with the actual image of the sample (figure \ref{fig:sample-3-image}), there is a small chip in the top left corner of the sample. The damage to the sample is almost certainly the main contributor of the increase in birefringence.

\begin{figure}[H]
	\centering
	\begin{subfigure}{0.49\textwidth}
	\centering
	\includegraphics[width = \textwidth]{baby-interferometer-left/rotation/intensity_aligned.png}
	\caption{Aligned Intensity\label{fig:sample-3-left-aligned}}
	\end{subfigure}
	\begin{subfigure}{0.49\textwidth}
	\centering
	\includegraphics[width = \textwidth]{baby-interferometer-left/rotation/intensity_antialigned.png}
	\caption{Antialigned Intensity\label{fig:sample-3-left-antialigned}}
	\end{subfigure}
	\caption{Baby Interferometer Sample 3 Left Intensity Plots\label{fig:sample-3-left-intensity}}
\end{figure}

\begin{figure}[H]
	\centering
	\begin{subfigure}{0.32\textwidth}
		\centering
		\includegraphics[width = \textwidth]{baby-interferometer-left/rotation/birefringence_alpha.png}
		\caption{Birefringence Axis Angle $\alpha$\label{fig:sample-3-left-alpha}}
	\end{subfigure}
	\begin{subfigure}{0.32\textwidth}
		\centering
		\includegraphics[width = \textwidth]{baby-interferometer-left/rotation/birefringence_beta.png}
		\caption{Birefringence Retardance $\beta$\label{fig:sample-3-left-beta}}
	\end{subfigure}
	\centering
	\begin{subfigure}{0.32\textwidth}
		\centering
		\includegraphics[width = \textwidth]{baby-interferometer-left/rotation/depolarization.png}
		\caption{Depolarization $D$\label{fig:sample-3-left-depolarization}}
	\end{subfigure}
	\caption{Baby Interferometer Sample 3 Left Plots\label{fig:sample-3-left-plots}}
\end{figure}

\begin{figure}[H]
	\centering
	\begin{subfigure}{0.49\textwidth}
	\centering
	\includegraphics[width = \textwidth]{baby-interferometer-right/rotation/intensity_aligned.png}
	\caption{Aligned Intensity\label{fig:sample-3-right-aligned}}
	\end{subfigure}
	\begin{subfigure}{0.49\textwidth}
	\centering
	\includegraphics[width = \textwidth]{baby-interferometer-right/rotation/intensity_antialigned.png}
	\caption{Antialigned Intensity\label{fig:sample-3-right-antialigned}}
	\end{subfigure}
	\caption{Baby Interferometer Sample 3 Right Intensity Plots\label{fig:sample-3-right-intensity}}
\end{figure}

\begin{figure}[H]
	\begin{subfigure}{0.3\textwidth}
		\centering
		\includegraphics[width = \textwidth]{baby-interferometer-right/rotation/birefringence_alpha.png}
		\caption{Birefringence Axis Angle $\alpha$\label{fig:sample-3-right-alpha}}
	\end{subfigure}
	\begin{subfigure}{0.34\textwidth}
		\centering
		\includegraphics[width = \textwidth]{baby-interferometer-right/rotation/birefringence_beta.png}
		\caption{Birefringence Retardance $\beta$\label{fig:sample-3-right-beta}}
	\end{subfigure}
	\centering
	\begin{subfigure}{0.34\textwidth}
		\centering
		\includegraphics[width = \textwidth]{baby-interferometer-right/rotation/depolarization.png}
		\caption{Depolarization $D$\label{fig:sample-3-right-depolarization}}
	\end{subfigure}
	\caption{Baby Interferometer Sample 3 Right Plots\label{fig:sample-3-right-plots}}
\end{figure}

\subsection{Fresnal Curves}

Fresnal curves are measured for all three silicon samples in an attempt to help with their alignment to the beam. These were collected by rotation the sample in the y-axis (up-down) at $0^\circ$, $+6^\circ$, and $-6^\circ$ x-axis rotation angles. These are done with the polarizer and analyzer both aligned and anti-aligned for a total of 6 measurements per sample. Unfortunately, there was little conclusive information that could be drawn from these plots, as shown in figure \ref{fig:fresnal-plots}. The expected oscillations are not present, nor is the expectations that power would diverge symmetrically away from the perpendicular angle. As a result, these measurements could not be used to assist in aligned the samples perpendicularly to the beam, but they are still included for completeness' sake. 

\begin{figure}[H]
	\begin{subfigure}{0.34\textwidth}
		\centering
		\includegraphics[width = \textwidth]{machined-sample-1-front/fresnal-curve/fresnal-curve-all.png}
		\caption{Thin Sample 1 Front\label{fig:sample-1-fresnal}}
	\end{subfigure}
	\begin{subfigure}{0.34\textwidth}
		\centering
		\includegraphics[width = \textwidth]{machined-sample-2/fresnal-curve/fresnal-curve-all.png}
		\caption{Wedge Sample 2$\beta$\label{fig:sample-2-fresnal}}
	\end{subfigure}
	\centering
	\begin{subfigure}{0.3\textwidth}
		\centering
		\includegraphics[width = \textwidth]{baby-interferometer/fresnal-curve/fresnal-curve-all.png}
		\caption{Baby Interferometer Sample 3\label{fig:sample-3-fresnal}}
	\end{subfigure}
	\caption{Silicon Sample Fresnal Plots\label{fig:fresnal-plots}}
\end{figure}

\section{Conclusion and Future Work}

This report details a method for measuring the birefringence of a silicon sample using polarized NIR light. It presents results for the measurements done on several silicon samples. It demonstrates the ability to measure the birefringence value of silicon, and to resolve certain features of the sample such as edges, changes in thickness and minor defects. However, this process still contends with certain unexplained observations such as the poor $T(\theta)$ fits at certain points. These poor fits to \eqref{Transmittance} has led to consequences that caused nearly all of the data of sample 2 to be invalid. 

The goal of the present work is to develop imaging techniques for evaluating materials used for optical purposes, specifically materials to use as gratings and for neutron interferometry. To a certain extent, this goal is met with the transmittance measurement technique, as many of the evident regions of stresses and strains in the sample were revealed through the scans. Though there is much work to be done to improve confidence in the model. 

Further work includes resolving the difficulties in fitting equation \eqref{Transmittance} with the data. This may require an improvement to the model, or a way to correct for systematic errors, such that poor fits like those in figures \ref{fig:sample-2-fit-bad-6} and \ref{fig:sample-2-fit-bad-20} can be avoided. This would improve confidence in the measured birefringence value.


% TODO (Tim): create citations and references. 
% \section{References}

% \bibliographystyle{plain}
% \bibliography{references}

\end{document}