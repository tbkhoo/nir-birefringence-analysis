https://www.thorlabs.com/newgrouppage9.cfm?objectgroup_id=7234
- Design Wavelength 	532 nm
- retardance: λ/4 
- accuracy: <λ/300
- material: Crystalline Quartz
- Unmounted Thickness: 	1.99 mm

https://www.thorlabs.com/newgrouppage9.cfm?objectgroup_id=7234
- "Thorlabs' Zero-Order Quarter-Wave Plates are built by combining two Multi-Order Crystalline Quartz Wave Plates to obtain an optical path length difference of λ/4. By aligning the fast axis of one plate with the slow axis of the other, the net result is a compound retarder whose exact retardance is the difference between each plate's individual retardance"
- "Air-Spaced Design"
- Appears to be two airspaced quartz waveplates (antialigned?) with a path difference appropriate for lambda. 


https://www.crystran.co.uk/optical-materials/quartz-crystal-sio2#
No = ordinary ray
Ne = extraordinary ray

$n^2-1=\frac{0.6961663λ^2}{λ^2-0.0684043^2}+\frac{0.4079426λ^2}{λ^2-0.1162414^2}+\frac{0.8974794λ^2}{λ^2-9.896161^2}$

$n=(1+0.6961663/(1-(0.0684043/x)**2)+0.4079426/(1-(0.1162414/x)**2)+0.8974794/(1-(9.896161/x)**2))**.5$


| Wavelength |  No   |  Ne   | $\delta$ n |
|:----------:|:-----:|:-----:|:----------:|
|   532nm    | 1.547 | 1.556 |  0.009198  |
|  1550nm    | 1.528 | 1.536 |  0.008455  |

DOI: 10.1016/S0030-3992/02/00147-0 

## Thickness of 532nm $\lambda /4$ waveplate
- $\beta = \lambda /4 = \pi/2$
- $\delta \beta <\lambda/300$
- $\lambda = 532nm$
- $\Delta n = 0.009[198]$
- $d = \frac {\beta \lambda} {2 \pi \Delta n}$
- $d = \frac {\left ( \frac {\pi}{2} \cdot 532 \right)} {2 \pi \cdot 0.009[198]}$
- $d = 1.4459665144596655\times10^{-5}$
- $d = 14.46 \pm 1.3\% \mu m$

https://www.newport.com/f/quartz-zero-order-quarter-wave-plates
- retardance changes based on angle
- but even at 10 degrees, its only about a 20% error, not the 100% we are seeing. 

$y = a + |b|\cos \left (\frac {2\pi} {p} \left( x - \phi \right ) \right)$

$D = 2 \frac {I_\perp} {I_\perp + I_\parallel} = 2T = \left (1 - \cos\beta \right) \sin^2 \theta$

$\sin^2 \left[\frac {\beta} {2} \right] \sin^2 \left[\left(\theta - \alpha \right) \cdot \frac {\pi} {90} \right]$