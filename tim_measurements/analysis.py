from cgi import print_exception
import numpy as np
import pandas as pd
from typing import Tuple
import qexpy as q


def within_expected(
    num: float,
    expected_value: float,
    tolerance: float = 0.1,
    isangle: bool = False,
) -> bool:
    """
    Returns True if num is within +/- tolerance range of expected_value.

    Args:
        num: Value to be checked if it is within tolerance range.
        expected_value: Expected value for num.
        tolerance: Acceptable range of error/uncertainty for values.
            Defaults to 0.1.
        isangle: When True will perform a wrap around 0-360 to normalize angles
            in degrees. Defaults to False.

    Returns:
        bool: True if within expected range, False otherwise.
    """
    if isangle:
        cond1 = (num + tolerance) % 360 - expected_value <= tolerance
        cond2 = (num - tolerance) % 360 - expected_value <= tolerance
        return cond1 or cond2

    return np.abs(num - expected_value) <= tolerance


def get_measurements(
    df: pd.DataFrame,
    column: str,
    filters: dict[str, float],
    tolerance: float = 0.1,
) -> np.ndarray:
    """
    Returns the power values in scan_df with matching conditions within specified tolerance.

    Args:
        df: Raw data from .csv file of stokes measurements.
        column: A string that represents the column name in df to fetch.
            Common values are: "Power - Mean [W]" and "Power - Std. [W]".
        filters: Keys are column names in df and values are values to match
            such that df[key] == value.
        tolerance: Acceptable range of error/uncertainty for values. Defaults to 0.1.

    Returns:
        Requested data that matches all specified conditions.
    """

    filter1 = np.ones(df.shape[0], dtype=bool)
    for key, val in filters.items():
        filter1 *= np.array(
            within_expected(df[key], val, tolerance=tolerance), dtype=bool
        )
    return df[column][filter1].values


def calc_stokes_params(
    df: pd.DataFrame,
    polarizer_angles: list[float],
    analyzer_angles: list[float],
    waveplate_angles: list[float],
    x: float = 0,
    y: float = 0,
    tolerance: float = 0.1,
) -> q.MeasurementArray:
    """
    Returns the computed stokes parameters S0, S1, S2, S3.

    https://pol3he.sites.wm.edu/wp-content/uploads/sites/50/2020/01/measuring-Stokes-parameters.pdf

    Note:
        polarizer_angles, analyzer_angles, and waveplate_angles must be
        of same length.They are expected to have a length of 4.

    Args:
        df: Raw data from .csv file of stokes measurements
        polarizer_angles: List of polarizer angles used for measurements.
        analyzer_angles: List of analyzer angles used for measurements.
        waveplate_angles: List of waveplate angles used for measurements.
        x: x position of the sample of measurement in pulse units.
        y: y position of the sample of measurement in pulse units.
        tolerance: Acceptable range of error/uncertainty for values.
            Defaults to 0.1.

    Returns:
        q.MeasurementArray with 4 stokes parameters and associated errors:
            s0, s1, s2, s3.
    """
    power = []
    error = []

    if len(polarizer_angles) != len(analyzer_angles) or (
        len(polarizer_angles) != len(waveplate_angles)
    ):
        raise ValueError(
            "polarizer_angles, analyzer_angles, "
            "and waveplate_angles must be of same length."
        )

    for polarizer_angle, analyzer_angle, waveplate_angle in zip(
        polarizer_angles, analyzer_angles, waveplate_angles
    ):

        filters_dict = {
            "Polarizer [deg]": polarizer_angle,
            "Analyzer [deg]": analyzer_angle,
            "Waveplate [deg]": waveplate_angle,
            "G3 X Translation": x,
            "G3 Y Translation": y,
        }

        power.append(
            get_measurements(
                df,
                "Power - Mean [W]",
                filters_dict,
                tolerance,
            )
        )
        error.append(
            get_measurements(
                df,
                "Power - Std. [W]",
                filters_dict,
                tolerance,
            )
        )

    power = np.array(np.squeeze(power))
    error = np.array(np.squeeze(error))
    # print("power: {}".format(power))
    # print("error:{}".format(error))

    m = q.MeasurementArray(power, error)

    # stokes parameters:
    s = []
    print("m1: {}, m2: {}, m3: {}, m4: {}".format(m[0], m[1], m[2], m[3]))
    s.append(m[0] + m[1])  # I
    s.append((m[0] - m[1]))  # Q
    s.append((2 * m[2] - s[0]))  # U
    s.append((s[0] - 2 * m[3]))  # V

    return s


def calc_birefrigence(s0: list, s45: list, sRHC: list) -> Tuple[float, float]:
    """Returns the birefringence values calculated from stokes parameters.

    Note:
        Args s0, s45, and sRHC should be lists obtained using the
        calc_stokes_params() method in this module.

    Args:
        s0 (list): stokes vectors from horizontally polarized light input.
        s45 (list): stokes vectors from diagonally polarized light input.
        sRHC (list): stokes vectors from circularly polarized light input.

    Returns:
        :rtype: (float, float)

        A tuple containing the two relevant parameters to birefrigence.

        alpha: birefrigence axis angle in radians.
        beta: retardance or 'birefrigence amount' in radians.
    """

    alpha = 1 / 2 * q.atan(-s0[3] / s45[3])
    beta = q.atan(s45[3] / (q.cos(2 * alpha) * sRHC[3]))

    return alpha, beta


def cosine(
    x: float | np.ndarray,
    p: float = 1,
    phi: float = 0,
    a: float = 0,
    b: float = 1,
) -> float | np.ndarray:
    """Returns the cosine of x with the specified fit parameters.

    Function is in the general form a + |b|cos(2pi/p(x-phi))
    where b is in absolute values so that phi must reflect a negative value.

    Note: The return type is identical to input type of theta.

    Args:
        x: x value(s) for the cosine function.
        p: Period of the function in radians. Defaults to 1 rad.
        phi: Phase shift of the function. Defaults to 0.
        a: Constant of the function. Defaults to 0.
        b: Amplitude of the function. Defaults to 1.
    """

    return a + b * q.cos(2 * np.pi / p * (x - phi))


def transmittance(
    theta: np.ndarray | list[float], beta: float, alpha: float, D: float
) -> np.ndarray | list[float]:
    """
    Returns the transmittance given the specified parameters.

    Note: The return type is identical to input type of theta.

    Args:
        theta: polarizer rotation angle (in deg).
        beta: birefringence phase shift (in rad).
        alpha: birefringence axis angle (in deg).
        D: depolarization (offset)
    """
    return D + (
        q.sin(beta / 2) * q.sin(2 * (theta - alpha) * np.pi / 180)
    ) ** 2 * (1 - D)


def slice_q(
    qArray: q.MeasurementArray, axis0: slice, axis1: slice, shape: tuple
) -> q.MeasurementArray:
    """
    Returns a sliced 2D qexpy MeasurementArray.

    Process is performed as follows:
        qArray[axis0, axis1]

    Args:
        qArray (qexpy.MeasurementArray): 2D MeasurementArray to be sliced
        axis0 (slice): slice object containing paramaters to slice in axis=0
        axis1 (slice): slice object containing paramaters to slice in axis=1
        shape (tuple): shape of 2D array
    """
    value = np.array(qArray.values).reshape(shape)
    error = np.array(qArray.errors).reshape(shape)

    value = value[axis0, axis1]
    error = error[axis0, axis1]
    newArray = q.MeasurementArray(value.flatten(), error.flatten())

    return newArray


def d_mm(d1: int, d2: int = 0, neutral_offset: float = 50) -> float:
    """Returns actual distance between grating 1 and 2 in mm.

    If only 1 distance is passed, it is interpreted as the sum of both kohzu 1 and 2.

    Args:
        d1 : z position of kohzu 1 in pulse units.
        d2 : z position of kohzu 2 in pulse units. Defaults to origin (0).
        neutral_offset: Distance between gratings with kohzu 1 and 2 at origin
        (0 pulse units). Must be non-negative.
    """

    return neutral_offset - (d1 + d2) * 0.25e-3


def main() -> None:
    import sys
    import qexpy as q

    sys.setrecursionlimit(100000)
    rng = np.random.default_rng()
    a = rng.standard_normal(1000)
    b = np.abs(rng.standard_normal(1000))
    c = q.MeasurementArray(a, b)
    c = np.asarray(c)
    try:
        d = c.reshape(10, 10, 10)
        e = d.mean()
        print(e)
    except RecursionError:
        print("error")
    finally:
        print("done")


if __name__ == "__main__":
    main()
