import numpy as np
import pandas as pd
from typing import Tuple


def within_expected(num: float, expected_value: float, tolerance: float = 0.1) -> bool:
    """
    Returns True if num is within +/- tolerance range of expected_value.
    """
    return np.abs(num - expected_value) <= tolerance


def get_measurements(
    scan_df: pd.DataFrame,
    polarizer_angle: float,
    analyzer_angle: float,
    waveplate_angle: float,
    x: float,
    y: float,
    tolerance: float = 0.1,
) -> pd.Series:
    """
    Returns the power values in scan_df with matching conditions within specified tolerance:
    polarizer_angle ['Polarizer [deg]'], analyzer_angle ['Analyzer [deg]'],
    waveplate_angle ['Waveplate [deg]'], x ['G3 X Translation'], y ['G3 Y Translation'].
    """

    measurement = scan_df["Power - Mean [W]"][
        np.array(
            within_expected(
                scan_df["Polarizer [deg]"].values, polarizer_angle, tolerance
            )
        )
        & np.array(
            within_expected(scan_df["Analyzer [deg]"].values, analyzer_angle, tolerance)
        )
        & np.array(
            within_expected(
                scan_df["Waveplate [deg]"].values, waveplate_angle, tolerance
            )
        )
        & np.array(within_expected(scan_df["G3 X Translation"].values, x, tolerance))
        & np.array(within_expected(scan_df["G3 Y Translation"].values, y, tolerance))
    ]
    if type(measurement) is pd.Series:
        return measurement.values
    else:
        return measurement


def calc_stokes_params(
    scan_df: pd.DataFrame,
    polarizer_angles: list = [0, 0, 0, 0],
    analyzer_angles: list = [174, 84, 129, 174],
    waveplate_angles: list = [5, 95, 50, 50],
    x: float = 0,
    y: float = 0,
    tolerance: float = 0.1,
) -> list:
    """
    Returns the computed stokes parameters S0, S1, S2, S3.
    polarizer_angles, analyzer_angles, and waveplate_angles must be of same length.

    Arguments
    ~~~~~~~~~
    scan_df: pandas DataFrame with raw data from .csv file
    polarizer_angles, analyzer_angles, waveplate_angles: lists of angles used for measurements.
    x, y: coordinates in pulse units of the sample of measurement.
    tolerance: acceptable range of error for values.
    """
    m = []

    if len(polarizer_angles) == len(analyzer_angles) and len(polarizer_angles) == len(
        waveplate_angles
    ):

        for polarizer_angle, analyzer_angle, waveplate_angle in zip(
            polarizer_angles, analyzer_angles, waveplate_angles
        ):
            m.append(
                get_measurements(
                    scan_df,
                    polarizer_angle,
                    analyzer_angle,
                    waveplate_angle,
                    x,
                    y,
                    tolerance,
                )
            )

        # stokes parameters: https://pol3he.sites.wm.edu/wp-content/uploads/sites/50/2020/01/measuring-Stokes-parameters.pdf
        s = []
        #         print("m1: {}, m2: {}, m3: {}, m4: {}".format(m[0], m[1], m[2], m[3]))
        s.append(m[0] + m[1])  # I
        s.append((m[0] - m[1]))  # Q
        s.append((2 * m[2] - s[0]))  # U
        s.append((s[0] - 2 * m[3]))  # V

        #         print("s:{}".format(s))

        return s

    else:
        raise ValueError(
            "polarizer_angles, analyzer_angles, and waveplate_angles must be of same length."
        )


def calc_birefrigence(s0: list, s45: list, sRHC: list) -> Tuple[float, float]:

    alpha = 1 / 2 * np.arctan(-s0[3] / s45[3])
    beta = np.arctan(s45[3] / (np.cos(2 * alpha) * sRHC[3]))

    #     while alpha < 0 or beta < 0:
    #         alpha += np.pi/2
    #         beta = np.arctan(s45[3] / (np.cos(2*alpha) * sRHC[3]))

    return alpha, beta


def cosine(
    x: np.ndarray, p: float = 1, phi: float = 0, a: float = 0, b: float = 1
) -> np.ndarray:
    """
    Returns the cosine of x with the specified fit parameters.
    """

    return a + abs(b) * np.cos(2 * np.pi / p * (x - phi))


def transmittance(theta: np.ndarray, eta: float) -> np.ndarray:
    """
    Returns the transmittance for specified parameters.

    Arguments
    ~~~~~~~~~
    theta: fast axis angle to polarization angle
    eta: birefrigence phase shift

    """


def d_mm(d1: int, d2: int = 0, neutral_offset: float = 50) -> float:
    """
    Returns actual distance between grating 1 and 2 in mm.

    Parameters
    ----------
    d1, d2: z position of kohzu 1 and 2 in pulse units.
            If only 1 distance is passed, it is interpreted as the sum of both kohzu 1 and 2.
    neutral_offset: Distance between gratings with kohzu 1 and 2 at origin (0 pulse units).
                    Must be non-negative.
    """

    return neutral_offset - (d1 + d2) * 0.25e-3


def main():
    print("done")


if __name__ == "__main__":
    main()
